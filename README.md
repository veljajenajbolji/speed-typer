# Speed typing practice tool #

This app is used to practice fast typing, by giving you random quotes to type out. If you type in the wrong letter you will have to re-do it.
### Technologies ###

Single-page app, using vanilla javascript and jquery, bootstrap for quick design purposes.

### How do I get set up? ###

Clone and run index.html in your browser. No need for a server.

### Author ###

Veljko Ilic